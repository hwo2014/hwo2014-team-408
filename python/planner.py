from heapq import *
from physics import *


MAX_ANGLE = 50


def normalize_map(map_info):
    pieces = map_info['race']['track']['pieces']
    for piece in pieces:
        if not 'length' in piece:
            piece['length'] = (2 * math.pi * piece['radius']) * piece['angle']/360.0

    for piece_idx, piece in enumerate(pieces):
        if not 'angle' in piece:
            continue

        if piece['radius'] == 200:
            piece['max_speed'] = 10.0
        else:            
            piece['max_speed'] = 6.5
        # maxspeed = max_speed()

        # while maxspeed > 0:
        #     working = True
        #     throttle = constant_speed_throttle(maxspeed)
        #     state = State(pieces, piece_idx=piece_idx, car_velocity=maxspeed, throttle=throttle)            
        #     while state.piece_idx == piece_idx:
        #         state = state.change_throttle(throttle)
        #         print state.in_piece_distance

        #         #print state.car_angle, state.car_angular_velocity
        #         if state.car_angle > MAX_ANGLE:
        #             working = False
        #             break

        #     if working:
        #         break

        #     maxspeed -= 0.1

        # piece['maxspeed'] = maxspeed
        # print piece
        # break



def build_plan(map_info, initial_state):
    state = initial_state

    max_speed = state.piece.get('max_speed')
    if max_speed:
        return constant_speed_throttle(max_speed)

    initial_plan = []
    while not state.piece.get('max_speed'):
        state = state.change_throttle(1)
        initial_plan.append(state)

    plan = initial_plan
    last_piece = state.piece

    while state.car_velocity > last_piece['max_speed']:
        initial_plan = initial_plan[:-1]

        #print [z.throttle for z in initial_plan], state.car_velocity, last_piece['max_speed']

        if not initial_plan:
            return 0

        plan = list(initial_plan)
        if plan:
            state = plan[-1]
        else:
            state = initial_state

        while state.piece != last_piece:
            try:
                state = state.change_throttle(0)
                plan.append(state)
            except KeyboardInterrupt:
                pass
                #import pudb;pu.db

    print [z.throttle for z in plan], state.car_velocity, last_piece['max_speed']

    return plan[0].throttle


from sortedcontainers import SortedList, SortedDict
constraints = SortedDict()


def build_plan2(map_info, initial_state):
    global constraints
    
    throttles = [1.0, 0.5, 0.0]

    bucket_size = 1

    to_go = SortedList([(0, initial_state)])

    max_plan = 0

    p1 = [0.0] * 17
    while to_go:
        cost, state = to_go.pop()

        state_cbucket = int(state.total_distance / bucket_size)
        state_cvelocity = constraints.get(state_cbucket, 999999)

        max_plan = max(0, len(state.plan))

        # if state.plan == p1:
        #     import pudb; pu.db

        if state_cvelocity < state.car_velocity:
            continue

        #print state.plan

        if len(state.plan) >= 30:
            return state.plan

        bfound = False
        for t in throttles:
            new_state = state.change_throttle(t)

            cbucket = int(new_state.total_distance / bucket_size)
            maxspeed = constraints.get(cbucket, 999999)

            if abs(new_state.car_angle) < 60 and new_state.car_velocity <= maxspeed:
                to_go.add((new_state.total_distance, new_state))
                bfound = True

        if not bfound and len(state.plan) == max_plan:
            constraints[state_cbucket] = min(state_cvelocity, state.car_velocity)
            dist = state.total_distance
            vel = constraints[state_cbucket]
            while dist > 0:
                prev_dist = dist - vel
                prev_vel = vel / 0.98
                cbucket = int(prev_dist / bucket_size)
                if prev_vel < constraints.get(cbucket, 999999):
                    constraints[cbucket] = prev_vel
                else:
                    break
                dist = prev_dist
                vel = prev_vel

            #print constraints
    print "No solution"
    return [0.0]



class State(object):
    def __init__(self, pieces, piece_idx=0, lap=0, lane=0, in_piece_distance=0.0, car_velocity=0.0, car_angle=0.0, car_angular_velocity=0.0, throttle=0, plan=[], total_distance=0.0):
        self.pieces = pieces
        self.piece_idx = piece_idx
        self.lap = lap
        self.lane = lane
        self.in_piece_distance = in_piece_distance
        self.car_velocity = car_velocity
        self.car_angle = car_angle
        self.car_angular_velocity = car_angular_velocity
        self.throttle = throttle

        self.plan = plan
        self.total_distance = total_distance

    @property
    def piece(self):
        return self.pieces[self.piece_idx]

    def change_throttle(self, throttle):
        car_acceleration = acceleration(throttle, self.car_velocity)
        new_velocity = self.car_velocity + car_acceleration

        new_distance = self.in_piece_distance + new_velocity
        total_distance = self.total_distance + new_distance

        lap = self.lap
        piece_idx = self.piece_idx
        last_piece = self.pieces[piece_idx]

        while new_distance >= self.pieces[piece_idx]['length']:
            new_distance -= self.pieces[piece_idx]['length']
            piece_idx += 1
            if piece_idx >= len(self.pieces):
                piece_idx = 0
                lap += 1

        aa = car_angular_acceleration(new_velocity, last_piece, self.car_angle, self.car_angular_velocity)
        car_angular_velocity = self.car_angular_velocity + aa
        car_angle = self.car_angle + car_angular_velocity

        return State(self.pieces, piece_idx, lap, self.lane, new_distance, new_velocity, car_angle, car_angular_velocity, throttle, plan=self.plan + [throttle], total_distance=total_distance)


if __name__ == '__main__':
    import simplejson
    with open('keimola.json') as f:
        track = simplejson.load(f)

        normalize_map(track)
        print track
        #print build_plan(track, State(track['race']['track']['pieces']))

        #s = State(track['race']['track']['pieces'], piece_idx=3, lap=0, in_piece_distance=29.130064933854243, car_velocity=7.568774185023383, car_angle=0.0, car_angular_velocity=0.0)
        #print build_plan(track, s)
        #print build_plan2(track, State(track['race']['track']['pieces']))
        s2 = State(track['race']['track']['pieces'], piece_idx=3, lap=0, in_piece_distance=65, car_velocity=7.568774185023383, car_angle=0.0, car_angular_velocity=0.0)
        print build_plan2(track, s2)



#73, 0, 3, "{u'length': 100.0, u'switch': True}", 100.0, 0, 0, 29.130064933854243, 0, 0, 7.568774185023383, 0.0, 0.0, 0.0

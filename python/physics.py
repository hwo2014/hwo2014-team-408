import math

engine_power = 0.2
drag_coef = 0.02
car_mass = 1

max_angle = 60
tire_friction_force = 1


def drag_force(velocity):
    return drag_coef * velocity 


def acceleration(throttle, current_velocity):
    engine_force = engine_power * throttle
    return (engine_force - drag_force(current_velocity)) / car_mass


def constant_speed_throttle(velocity):
    return 0.1 * velocity


def max_speed():
    return 10.0


def car_angular_acceleration(velocity, last_piece, car_angle, car_angular_velocity):
    if 'radius' in last_piece:
        centri_force = velocity/last_piece['radius']

        if last_piece['angle'] < 0:
            centri_force = -centri_force
    else:
        centri_force = 0

    if centri_force > -0.04 and centri_force < 0.04:
        centri_force = 0

    friction_force = -car_angular_velocity

    normalization_force = math.sin(car_angle*math.pi/180)

    return 0.89994 * normalization_force + 0.09812 * friction_force + 12.88817 * centri_force



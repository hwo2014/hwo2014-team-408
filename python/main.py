import json
import socket
import sys
from planner import *


class NoobBot(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key
        self.lastPosition = None
        self.lastVelocity = 0.0
        self.lastAngle = 0
        self.lastAngularVelocity = 0
        self.tick = -1
        self.found = False

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.sendall(msg + "\n")

    def join(self):
        return self.msg("join", {"name": self.name,
                                 "key": self.key})

    def createRace(self, trackName, carCount=1, password='pwd1'):
        data = {
            'botId': {"name": self.name,
                      "key": self.key},
            'trackName': trackName,
            'carCount': carCount,
            'password': password
        }
        return self.msg('createRace', data)

    def throttle(self, throttle):
        self.msg("throttle", throttle)

    def switchLane(self, direction):
        assert direction in ('Left', 'Right')
        self.msg('switchLane', direction)

    def ping(self):
        self.msg("ping", {})

    def run(self):
        self.join()
        #self.createRace('usa')
        #self.createRace('keimola')
        #self.createRace('germany')
        
        self.msg_loop()

    def on_join(self, data):
        print("Joined")
        print data
        self.ping()

    def on_game_init(self, data):
        print("Game init")
        self.game_data = data
        print data
        normalize_map(self.game_data)

    def on_game_start(self, data):
        print("Race started")
        print data
        self.ping()

    def on_car_positions(self, data):
        #print data
        self.tick += 1
        pp = data[0]['piecePosition']
        pieceIndex = pp['pieceIndex']
        inPieceDistance = pp['inPieceDistance']
        lane_position = pp['lane']
        lap = pp['lap']
        start_lane = lane_position['startLaneIndex']
        end_lane = lane_position['endLaneIndex']
        angle = data[0]['angle']
        piece = self.game_data['race']['track']['pieces'][pieceIndex]

        if self.lastPosition:
            if self.lastPosition['pieceIndex'] == pieceIndex:
                self.velocity = inPieceDistance - self.lastPosition['inPieceDistance']
        else:
            self.velocity = inPieceDistance

        angularVelocity = angle - self.lastAngle
        angularAcceleration = angularVelocity - self.lastAngularVelocity

        #print (self.velocity - self.lastVelocity)
        length = piece.get('length', 0)
        radius = piece.get('radius', 0)
        piece_angle = piece.get('angle', 0)
        print '%r, %r, %r, "%r", %r, %r, %r, %r, %r, %r, %r, %r, %r, %r' % (self.tick, lap, pieceIndex, piece, length, radius, piece_angle, inPieceDistance, start_lane, end_lane, self.velocity, angle, angularVelocity, angularAcceleration)

        self.lastVelocity = self.velocity
        self.lastAngularVelocity = angularVelocity
        self.lastPosition = pp
        self.lastAngle = angle

        # if pieceIndex == 1 and lap == 0:        
        #     self.switchLane('Right')
        # else:
        #self.throttle(1)
        print "Planning... ",
        plan = [build_plan(self.game_data, State(self.game_data['race']['track']['pieces'], piece_idx=pieceIndex, lap=lap, in_piece_distance=inPieceDistance, car_velocity=self.velocity, car_angle=angle, car_angular_velocity=angularVelocity))]
        #plan = build_plan2(self.game_data, State(self.game_data['race']['track']['pieces'], piece_idx=pieceIndex, lap=lap, in_piece_distance=inPieceDistance, car_velocity=self.velocity, car_angle=angle, car_angular_velocity=angularVelocity))
        print plan
        self.throttle(plan[0])
        #if self.tick == 1:
        #self.throttle(1)

        # if self.tick in (100, ):
        #     print "Switching lane"
        #     self.switchLane('Right')
        # if piece.get('switch'):
        #     print "Left"
        # else:
        #     self.ping()

    def on_crash(self, data):
        print("Someone crashed")
        print data
        #self.ping()

    def on_game_end(self, data):
        print("Race ended")
        print data
        self.ping()

    def on_error(self, data):
        print("Error: {0}".format(data))
        self.ping()

    def msg_loop(self):
        msg_map = {
            'join': self.on_join,
            'gameInit': self.on_game_init,
            'gameStart': self.on_game_start,
            'carPositions': self.on_car_positions,
            'crash': self.on_crash,
            'gameEnd': self.on_game_end,
            'error': self.on_error,
        }
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data = msg['msgType'], msg['data']
            if msg_type in msg_map:
                msg_map[msg_type](data)
            else:
                pass
                print("Got {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = NoobBot(s, name, key)
        bot.run()


####
#
